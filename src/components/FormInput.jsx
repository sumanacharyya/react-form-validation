import React, { useState } from "react";
import "./formInput.css";

const FormInput = (props) => {
  const [focused, setFocused] = useState(false);
  const { label, errorMessage, onChange, id, ...inputProps } = props;

  const focusHandler = (e) => {
    setFocused(true);
  };

  return (
    <div className="formInput">
      <label htmlFor="">{label}</label>
      <input
        {...inputProps}
        onChange={onChange}
        onBlur={focusHandler}
        onFocus={() =>
          inputProps.name === "confirmPassword" && setFocused(true)
        }
        focused={focused.toString()}
      />
      <span>{errorMessage}</span>
    </div>
  );
};

export default FormInput;
