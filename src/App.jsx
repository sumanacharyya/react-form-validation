import { useState } from "react";
import reactLogo from "./assets/react.svg";
import FormInput from "./components/FormInput";
import "./App.css";

const initialValues = {
  username: "",
  email: "",
  dob: "",
  password: "",
  confirmPassword: "",
};

function App() {
  const [values, setValues] = useState(() => initialValues);

  const inputs = [
    {
      id: 1,
      name: "username",
      type: "text",
      placeholder: "Username",
      label: "Username",
      errorMessage:
        "Username includes 3-16 characters and not special characters!",
      pattern: "^[a-zA-Z0-9]{3,16}$",
      required: true,
    },
    {
      id: 2,
      name: "email",
      type: "email",
      placeholder: "Email",
      label: "Email",
      errorMessage: "Enter a valid email address",
      required: true,
    },
    {
      id: 3,
      name: "dob",
      type: "date",
      placeholder: "DOB",
      label: "DOB",
      errorMessage: "Enter a DOB",
      required: true,
    },
    {
      id: 4,
      name: "password",
      type: "password",
      placeholder: "Password",
      label: "Password",
      errorMessage:
        "Password should be under 8-16 characters and includes 1 letter, 1 number and 1 special character",
      pattern: `^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$`,
      required: true,
    },
    {
      id: 5,
      name: "confirmPassword",
      type: "password",
      placeholder: "Confirm Password",
      label: "Confirm Password",
      errorMessage: "Confirm password not matched",
      pattern: values.password,
      required: true,
    },
  ];

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    console.log(values);
  };

  return (
    <div className="App">
      <form onSubmit={submitHandler}>
        <h1>Register</h1>
        {inputs.map((input) => (
          <FormInput
            key={input.id}
            {...input}
            value={values[input.name]}
            onChange={onChange}
          />
        ))}
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default App;
